
object Ex2 extends App {

  val firstOperand = "GAIN"
  val secondOperand = "MORE"
  val result = "MONEY"

  val zero = ' '

  val distinctValues = "SENDMORY".toSet

  println("ex2")
  val start = System.currentTimeMillis()
  solve.foreach(println)
  val elapsed = System.currentTimeMillis() - start
  println(s"Elapsed time ms: $elapsed")

  def solve: Seq[Map[Char, Int]] = {
    val operands: Seq[(Char, Char)] = firstOperand.reverse.zipAll(secondOperand.reverse, zero, zero)
    val operandsAndResult: Seq[((Char, Char), Char)] = operands.zipAll(result.reverse, (zero, zero), zero)

    operandsAndResult.foldLeft(Seq(State(Map(zero -> 0), 0))) {
      case (states, ((op1, op2), res)) =>
        states.flatMap(_.nextStates(op1, op2, res))
    }.filter(s => s.carry == 0).map(_.mapping)
  }

  case class State(mapping: Map[Char, Int], carry: Int) {

    def nextStates(op1: Char, op2: Char, res: Char): Seq[State] = {
      val newChars: Set[Char] = Set(op1, op2, res).diff(mapping.keySet)

      val candidates: Seq[Map[Char, Int]] = newChars.foldLeft(Seq(mapping)){
        (candidates, char) =>
          for {
            value <- 0 to 9
            candidate <- candidates
            newCandidate = candidate + (char -> value) if areValuesDistinct(newCandidate)
          } yield newCandidate
      }

      for {
        mapping <- candidates if (mapping(op1) + mapping(op2) + carry) % 10 == mapping(res)
        newCarry = (mapping(op1) + mapping(op2) + carry) / 10
      } yield State(mapping, newCarry)
    }

    private def areValuesDistinct(candidate: Map[Char, Int]): Boolean = {
      val shouldBeDistinct = candidate.keySet.intersect(distinctValues)
      shouldBeDistinct.map(candidate).size == shouldBeDistinct.size
    }
  }

}
