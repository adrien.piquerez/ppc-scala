object Ex1 extends App {

  println("Ex1")
  val start = System.currentTimeMillis()
  solve.foreach(println)
  val elapsed = System.currentTimeMillis() - start
  println(s"Elapsed time ms: $elapsed")

  def solve: Seq[(Double, Int)] = {
    for {
      y <- Seq(2, 3, 8)
      x = 4D / y
      if x < 5 && y < 5
    } yield (x, y)
  }

}
