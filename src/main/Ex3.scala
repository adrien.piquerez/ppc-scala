
object Ex3 extends App {
  val input =
    """|0 0 0 2 0 0 0 0 0
       |0 8 0 0 3 0 0 7 0
       |3 0 0 5 0 4 0 0 0
       |0 0 0 0 0 0 0 2 8
       |8 3 0 0 1 0 0 0 0
       |0 4 0 7 2 0 3 5 1
       |0 7 0 0 5 6 0 0 4
       |0 0 3 0 0 0 0 0 0
       |2 0 5 4 0 1 6 0 3
    """.stripMargin

  val start = System.currentTimeMillis()
  solve.foreach(println)
  val elapsed = System.currentTimeMillis() - start
  println(s"Elapsed time ms: $elapsed")

  def solve: Seq[Grid] = {
    val inputArray = input.split("\r\n").map(s => s.split(" ").map(_.toInt))

    val filledValues = for {
      row <- 0 until 9; col <- 0 until 9
      value = inputArray(row)(col) if value != 0
    } yield (row, col) -> value

    val inputGrid: Option[Grid] = filledValues.foldLeft(Option(Grid())){
      case (gridOpt, ((row, col), value)) => gridOpt.flatMap(_.add(row, col, value))
    }

    val emptyValues = for {
      row <- 0 until 9; col <- 0 until 9
      if inputArray(row)(col) == 0
    } yield (row, col)

    emptyValues.foldLeft(inputGrid.toSeq) {
      case (seq, (row, col)) => for {
        grid <- seq
        value <- 1 to 9
        newGrid <- grid.add(row, col, value)
      } yield newGrid
    }
  }

}

object Grid {
  def apply(): Grid = {
    val rows = (0 until 9).map(_ -> Set.empty[Int])
    val cols = (0 until 9).map(_ -> Set.empty[Int])
    val blocks = for {row <- 0 until 3; col <- 0 until 3} yield (row, col) -> Set.empty[Int]
    new Grid(Map())(rows.toMap, cols.toMap, blocks.toMap)
  }
}

case class Grid private (map: Map[(Int, Int), Int])(rows: Map[Int, Set[Int]], cols: Map[Int, Set[Int]], blocks: Map[(Int, Int), Set[Int]]) {
  def add(row: Int, col: Int, value: Int): Option[Grid] = {
    if(rows(row).contains(value) || cols(col).contains(value) || blocks((row / 3, col / 3)).contains(value))
      None
    else Some(new Grid(map + ((row, col) -> value))(
      rows.updated(row, rows(row) + value),
      cols.updated(col, cols(col) + value),
      blocks.updated((row / 3, col / 3), blocks(row / 3, col / 3) + value)
    ))
  }

  override def toString: String = {
    (0 until 9).map(row => (0 until 9).map(col => map.get(row, col).getOrElse(0)).mkString(" ")).mkString("\n")
  }
}
