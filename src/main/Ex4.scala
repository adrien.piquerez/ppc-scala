import scala.collection.mutable

object Ex4 extends App {
  val inputCapacity = 34

  val foods = Seq(Food(6, 7), Food(4, 5), Food(2, 3))
  val cache = mutable.Map[Int, Int]()

  val start = System.currentTimeMillis()
  println(solve(inputCapacity))
  val elapsed = System.currentTimeMillis() - start
  println(s"Elapsed time ms: $elapsed")

  def solve(capacity: Int): Int = {
    if (cache.contains(capacity)) cache(capacity)
    else {
      val candidates = for (
        Food(energy, size) <- foods if size <= capacity
      ) yield energy + solve(capacity - size)

      val result = if(candidates.isEmpty) 0 else candidates.max
      cache += (capacity -> result)
      result
    }
  }

  case class Food(energy: Int, size: Int)

}
